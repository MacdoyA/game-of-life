import NavbarSetUpComponent from '@/primary/component/navbarSetUp/NavbarSetUp.component';
import NavbarSetUpVue from '@/primary/component/navbarSetUp/NavbarSetUp.vue';

export { NavbarSetUpComponent, NavbarSetUpVue };
