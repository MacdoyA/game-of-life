import { Options, Vue } from 'vue-class-component';
import { Emit } from 'vue-property-decorator';

@Options({})
export default class NavbarSetUpComponent extends Vue {
  open = true;

  swapOpen() {
    this.open = !this.open;
  }

  @Emit()
  addColumn() {}

  @Emit()
  deleteColumn() {}

  @Emit()
  addLine() {}

  @Emit()
  deleteLine() {}

  @Emit()
  startGame() {}
}
