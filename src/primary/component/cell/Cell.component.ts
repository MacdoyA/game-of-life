import { Options, Vue } from 'vue-class-component';
import { Prop } from 'vue-property-decorator';
import { Cell } from '@/domain/cell/Cell';

@Options({})
export default class CellComponent extends Vue {
  @Prop({ required: true })
  private cell!: Cell;
}
