import CellComponent from '@/primary/component/cell/Cell.component';
import CellVue from '@/primary/component/cell/Cell.vue';

export { CellComponent, CellVue };
