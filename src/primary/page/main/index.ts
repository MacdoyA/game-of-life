import MainPageComponent from '@/primary/page/main/MainPage.component';
import MainPageVue from '@/primary/page/main/MainPage.vue';

export { MainPageComponent, MainPageVue };
