import { Options, Vue } from 'vue-class-component';
import { CellVue } from '@/primary/component/cell';
import { emptyGrid, Grid } from '@/domain/grid/grid';
import { Inject } from 'vue-property-decorator';
import { GridRepository } from '@/domain/grid/gridRepository';

@Options({
  components: {
    CellVue,
  },
})
export default class MainPageComponent extends Vue {
  grid: Grid = emptyGrid();
  hideBorder = true;
  interval = 200;

  @Inject()
  private gridRepository!: () => GridRepository;

  async created() {
    this.grid = this.gridRepository().getGrid();
    setInterval(() => {
      this.gridRepository()
        .getNextGeneration()
        .then(grid => (this.grid.cells = grid.cells));
    }, this.interval);
  }
}
