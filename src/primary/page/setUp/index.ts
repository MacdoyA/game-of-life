import SetUpPageComponent from '@/primary/page/setUp/SetUpPage.component';
import SetUpPageVue from '@/primary/page/setUp/SetUpPage.vue';

export { SetUpPageComponent, SetUpPageVue };
