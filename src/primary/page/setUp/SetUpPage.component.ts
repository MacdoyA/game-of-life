import { Options, Vue } from 'vue-class-component';
import { CellVue } from '@/primary/component/cell';
import { emptyGrid, Grid } from '@/domain/grid/grid';
import { Inject } from 'vue-property-decorator';
import { GridRepository } from '@/domain/grid/gridRepository';
import { Cell, deadCell } from '@/domain/cell/Cell';
import { NavbarSetUpVue } from '@/primary/component/navbarSetUp';

@Options({
  components: {
    CellVue,
    NavbarSetUpVue,
  },
})
export default class SetUpPageComponent extends Vue {
  grid: Grid = emptyGrid();
  width = 3;
  height = 3;

  @Inject()
  private gridRepository!: () => GridRepository;

  async startGame() {
    await this.gridRepository().initGrid(this.grid);
    this.$router.push({ name: 'main' });
  }

  switchCell(cell: Cell) {
    cell.alive = !cell.alive;
  }

  addColumn() {
    this.width++;
    this.grid.cells.forEach(line => line.push(deadCell()));
  }

  deleteColumn() {
    this.width--;
    this.grid.cells.forEach(line => line.pop());
  }

  addLine() {
    this.height++;
    const line: Cell[] = [];
    this.grid.cells[0].forEach(() => line.push(deadCell()));
    this.grid.cells.push(line);
  }

  deleteLine() {
    this.height--;
    this.grid.cells.pop();
  }
}
