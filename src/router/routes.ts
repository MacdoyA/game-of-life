import { RouteRecordRaw } from 'vue-router';
import { MainPageVue } from '@/primary/page/main';
import { SetUpPageVue } from '@/primary/page/setUp';

export const routes: Array<RouteRecordRaw> = [
  {
    path: '/game',
    name: 'main',
    component: MainPageVue,
  },
  {
    path: '',
    name: 'setUp',
    component: SetUpPageVue,
  },
];
