import { GridRepository } from '@/domain/grid/gridRepository';
import { emptyBigGrid, Grid } from '@/domain/grid/grid';
import { AxiosInstance, AxiosResponse } from 'axios';
import { RestGrid, toGrid } from '@/secondary/grid/RestGrid';

const initGrid = (grid: Grid) =>
  'Generation 1:\n' +
  grid.cells.length +
  ' ' +
  grid.cells[0].length +
  '\n' +
  grid.cells.map(line => line.map(cell => (cell.alive ? '*' : '.')).join('')).join('\n');

export class RestGridRepository implements GridRepository {
  grid: Grid = emptyBigGrid();

  constructor(private axiosInstance: AxiosInstance) {}

  getGrid(): Grid {
    return this.grid;
  }

  async initGrid(grid: Grid): Promise<void> {
    await this.axiosInstance.post('/gameoflife/init', {
      init: initGrid(grid),
    });
    this.grid = grid;
  }

  async getNextGeneration(): Promise<Grid> {
    return this.axiosInstance.get('/gameoflife/next').then((response: AxiosResponse<RestGrid>) => toGrid(response.data));
  }
}
