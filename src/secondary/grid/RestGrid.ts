import { Grid } from '@/domain/grid/grid';
import { aliveCell, Cell, deadCell } from '@/domain/cell/Cell';

export type RestGrid = string;

export const toGrid = (data: RestGrid): Grid => {
  const grid: Grid = { cells: [] };
  data
    .split('\n')
    .filter((line, index) => index > 1)
    .forEach(line => {
      const l: Cell[] = [];
      for (let i = 0; i < line.length; i++) l.push(line.charAt(i) == '*' ? aliveCell() : deadCell());
      grid.cells.push(l);
    });
  return grid;
};
