export interface Cell {
  alive: boolean;
}

export const aliveCell = (): Cell => ({
  alive: true,
});

export const deadCell = (): Cell => ({
  alive: false,
});
