import { Cell, deadCell } from '@/domain/cell/Cell';

export interface Grid {
  cells: Cell[][];
}

export const emptyGrid = (): Grid => ({
  cells: [
    [deadCell(), deadCell(), deadCell()],
    [deadCell(), deadCell(), deadCell()],
    [deadCell(), deadCell(), deadCell()],
  ],
});

export const emptyBigGrid = (): Grid => ({
  cells: [
    [
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
    ],
    [
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
    ],
    [
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
    ],
    [
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
    ],
    [
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
    ],
    [
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
    ],
    [
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
      deadCell(),
    ],
  ],
});
