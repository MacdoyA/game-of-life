import { Grid } from '@/domain/grid/grid';

export interface GridRepository {
  getGrid(): Grid;
  initGrid(grid: Grid): Promise<void>;
  getNextGeneration(): Promise<Grid>;
}
