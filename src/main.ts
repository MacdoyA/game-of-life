import { createApp } from 'vue';
import { AppVue } from './primary/app';
import router from './router';
import store from './store';
import { RestGridRepository } from '@/secondary/grid/RestGridRepository';
import axios, { AxiosInstance } from 'axios';

const app = createApp(AppVue).use(store).use(router);

axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';

const axiosInstance: AxiosInstance = axios.create({
  baseURL: `${process.env.VUE_APP_BACKEND_BASE_URL}/api`,
});

const gridRepository = new RestGridRepository(axiosInstance);

app.provide('gridRepository', () => gridRepository);

app.mount('#app');
