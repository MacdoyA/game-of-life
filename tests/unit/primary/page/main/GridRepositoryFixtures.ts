import sinon, { SinonStub } from 'sinon';
import { GridRepository } from '@/domain/grid/gridRepository';
import { emptyBigGrid } from '@/domain/grid/grid';

export interface GridRepositoryStub extends GridRepository {
  getGrid: SinonStub;
  initGrid: SinonStub;
  getNextGeneration: SinonStub;
}

export const stubGridRepository = (): GridRepositoryStub => ({
  getGrid: sinon.stub(),
  initGrid: sinon.stub(),
  getNextGeneration: sinon.stub(),
});

export const defaultGridRepository = (): GridRepositoryStub => {
  const gridRepositoryStub = stubGridRepository();
  gridRepositoryStub.getGrid.returns(emptyBigGrid());
  gridRepositoryStub.getNextGeneration.resolves(emptyBigGrid());
  return gridRepositoryStub;
};
