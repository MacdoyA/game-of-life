import { flushPromises, shallowMount, VueWrapper } from '@vue/test-utils';
import { describe, expect, it, vi } from 'vitest';

import { MainPageComponent, MainPageVue } from '@/primary/page/main';
import { defaultGridRepository, GridRepositoryStub } from './GridRepositoryFixtures';
import { emptyBigGrid } from '@/domain/grid/grid';

let wrapper: VueWrapper<MainPageComponent>;
let component: MainPageComponent;

const wrap = (overrideGridRepository?: GridRepositoryStub) => {
  const gridRepository = overrideGridRepository ? overrideGridRepository : defaultGridRepository();
  wrapper = shallowMount(MainPageVue, {
    global: {
      provide: {
        gridRepository: () => gridRepository,
      },
    },
  });
  component = wrapper.vm;
};

describe('MainPage', () => {
  it('Should exists', () => {
    wrap();

    expect(wrapper.exists()).toBeTruthy();
  });

  it('Should get grid', async () => {
    wrap();
    await flushPromises();

    expect(component.grid).toEqual(emptyBigGrid());
  });

  it('Should get next generation grid every 200ms', async () => {
    vi.useFakeTimers();
    const gridRepository = defaultGridRepository();

    wrap(gridRepository);
    await flushPromises();
    vi.advanceTimersByTime(250);
    await flushPromises();

    expect(component.grid).toEqual(emptyBigGrid());

    expect(gridRepository.getNextGeneration.getCalls().length).toBe(1);

    vi.advanceTimersByTime(2000);
    await flushPromises();

    expect(gridRepository.getNextGeneration.getCalls().length).toBe(11);
  });
});
