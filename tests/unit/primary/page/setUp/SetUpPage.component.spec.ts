import { shallowMount, VueWrapper } from '@vue/test-utils';
import { describe, expect, it, vi } from 'vitest';

import { SetUpPageComponent, SetUpPageVue } from '@/primary/page/setUp';
import { defaultGridRepository, GridRepositoryStub } from '../main/GridRepositoryFixtures';
import { deadCell } from '@/domain/cell/Cell';
import { emptyGrid } from '@/domain/grid/grid';

let wrapper: VueWrapper<SetUpPageComponent>;
let component: SetUpPageComponent;

const wrap = (overrideGridRepository?: GridRepositoryStub) => {
  const gridRepository = overrideGridRepository ? overrideGridRepository : defaultGridRepository();
  const mockRouter = {
    push: vi.fn(),
  };

  wrapper = shallowMount(SetUpPageVue, {
    global: {
      provide: {
        gridRepository: () => gridRepository,
      },
      mocks: {
        $router: mockRouter,
      },
    },
  });
  component = wrapper.vm;
};

describe('SetUpPage', () => {
  it('Should exists', () => {
    wrap();

    expect(wrapper.exists()).toBeTruthy();
  });

  it('Should start game', async () => {
    const gridRepository = defaultGridRepository();
    wrap(gridRepository);

    await component.startGame();

    expect(gridRepository.initGrid.getCall(0).args[0]).toEqual(emptyGrid());
    expect(component.$router.push).toHaveBeenCalledWith({ name: 'main' });
  });

  it('Should switch cell', () => {
    const cell = deadCell();
    wrap();

    component.switchCell(cell);

    expect(cell.alive).toBeTruthy();
  });

  it('Should add column', () => {
    wrap();

    component.addColumn();

    expect(component.grid).toEqual({
      cells: [
        [deadCell(), deadCell(), deadCell(), deadCell()],
        [deadCell(), deadCell(), deadCell(), deadCell()],
        [deadCell(), deadCell(), deadCell(), deadCell()],
      ],
    });
  });

  it('Should delete column', () => {
    wrap();

    component.deleteColumn();

    expect(component.grid).toEqual({
      cells: [
        [deadCell(), deadCell()],
        [deadCell(), deadCell()],
        [deadCell(), deadCell()],
      ],
    });
  });

  it('Should add line', () => {
    wrap();

    component.addLine();

    expect(component.grid).toEqual({
      cells: [
        [deadCell(), deadCell(), deadCell()],
        [deadCell(), deadCell(), deadCell()],
        [deadCell(), deadCell(), deadCell()],
        [deadCell(), deadCell(), deadCell()],
      ],
    });
  });

  it('Should delete line', () => {
    wrap();

    component.deleteLine();

    expect(component.grid).toEqual({
      cells: [
        [deadCell(), deadCell(), deadCell()],
        [deadCell(), deadCell(), deadCell()],
      ],
    });
  });
});
