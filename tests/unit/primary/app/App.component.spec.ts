import { shallowMount, VueWrapper } from '@vue/test-utils';
import { describe, expect, it } from 'vitest';

import { AppComponent, AppVue } from '@/primary/app';

let wrapper: VueWrapper<AppComponent>;
let component: AppComponent;

const wrap = () => {
  wrapper = shallowMount(AppVue, {
    global: {
      stubs: ['router-view', 'router-link'],
    },
  });
  component = wrapper.vm;
};

describe('App', () => {
  it('Should exists', () => {
    wrap();

    expect(wrapper.exists()).toBeTruthy();
  });
});
