import { shallowMount, VueWrapper } from '@vue/test-utils';
import { describe, expect, it } from 'vitest';

import { NavbarSetUpComponent, NavbarSetUpVue } from '@/primary/component/navbarSetUp';

let wrapper: VueWrapper<NavbarSetUpComponent>;
let component: NavbarSetUpComponent;

const wrap = () => {
  wrapper = shallowMount(NavbarSetUpVue, {});
  component = wrapper.vm;
};

describe('NavbarSetUp', () => {
  it('Should exists', () => {
    wrap();

    expect(wrapper.exists()).toBeTruthy();
  });

  it('Should swap open', () => {
    wrap();

    component.swapOpen();

    expect(component.open).toBeFalsy();

    component.swapOpen();

    expect(component.open).toBeTruthy();
  });

  it('Should add column', () => {
    wrap();

    component.addColumn();

    expect(wrapper.emitted()).toBeTruthy();
  });

  it('Should delete column', () => {
    wrap();

    component.deleteColumn();

    expect(wrapper.emitted()).toBeTruthy();
  });

  it('Should add line', () => {
    wrap();

    component.addLine();

    expect(wrapper.emitted()).toBeTruthy();
  });

  it('Should delete line', () => {
    wrap();

    component.deleteLine();

    expect(wrapper.emitted()).toBeTruthy();
  });

  it('Should start game', () => {
    wrap();

    component.startGame();

    expect(wrapper.emitted()).toBeTruthy();
  });
});
