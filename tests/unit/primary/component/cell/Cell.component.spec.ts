import { shallowMount, VueWrapper } from '@vue/test-utils';
import { describe, expect, it } from 'vitest';

import { CellComponent, CellVue } from '@/primary/component/cell';
import { aliveCell } from '@/domain/cell/Cell';

let wrapper: VueWrapper<CellComponent>;
let component: CellComponent;

const wrap = () => {
  wrapper = shallowMount(CellVue, {
    props: {
      cell: aliveCell(),
    },
  });
  component = wrapper.vm;
};

describe('Cell', () => {
  it('Should exists', () => {
    wrap();

    expect(wrapper.exists()).toBeTruthy();
  });
});
