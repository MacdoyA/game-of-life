import { describe, expect, it } from 'vitest';
import { stubAxiosInstance } from '../../TestUtils';
import { RestGridRepository } from '@/secondary/grid/RestGridRepository';
import { emptyBigGrid, Grid } from '@/domain/grid/grid';
import { aliveCell, deadCell } from '@/domain/cell/Cell';

describe('RestGridRepository', () => {
  it('Should get grid', () => {
    const axiosInstanceStub = stubAxiosInstance();
    const restGridRepository = new RestGridRepository(axiosInstanceStub);

    expect(restGridRepository.getGrid()).toEqual(emptyBigGrid());
  });

  it('Should init grid', async () => {
    const axiosInstanceStub = stubAxiosInstance();
    const restGridRepository = new RestGridRepository(axiosInstanceStub);
    const grid: Grid = {
      cells: [
        [aliveCell(), deadCell(), deadCell(), deadCell()],
        [deadCell(), aliveCell(), deadCell(), deadCell()],
        [deadCell(), deadCell(), aliveCell(), deadCell()],
      ],
    };

    await restGridRepository.initGrid(grid);

    expect(axiosInstanceStub.post.getCall(0).args[0]).toEqual('/gameoflife/init');
    expect(axiosInstanceStub.post.getCall(0).args[1]).toEqual({ init: 'Generation 1:\n' + '3 4\n' + '*...\n' + '.*..\n' + '..*.' });
    expect(restGridRepository.getGrid()).toEqual(grid);
  });

  it('Should get next generation', async () => {
    const axiosInstanceStub = stubAxiosInstance();
    const restGridRepository = new RestGridRepository(axiosInstanceStub);
    const grid: Grid = {
      cells: [
        [aliveCell(), deadCell(), deadCell(), deadCell()],
        [deadCell(), aliveCell(), deadCell(), deadCell()],
        [deadCell(), deadCell(), aliveCell(), deadCell()],
      ],
    };
    axiosInstanceStub.get.resolves({ data: 'Generation 1:\n' + '3 4\n' + '*...\n' + '.*..\n' + '..*.' });

    expect(await restGridRepository.getNextGeneration()).toEqual(grid);
    expect(axiosInstanceStub.get.getCall(0).args[0]).toBeTruthy();
  });
});
